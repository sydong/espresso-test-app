package com.example.voyager.testapp.sample.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.constants.Constants;
import com.example.voyager.testapp.sample.db.ItemsDataSource;

import java.sql.SQLException;

/**
 * Created by voyager on 11/6/15.
 */
public class ItemDetailsActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView textViewItemNameDetail;
    private TextView textViewItemQuantityDetail;

    private Button buttonDeleteItem;

    private int itemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        String itemName = getIntent().getStringExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_NAME_EXTRA);
        String itemQuantity = String.valueOf(getIntent().getIntExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_QUANTITY_EXTRA, -1));

        itemId = getIntent().getIntExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_ID_EXTRA, -1);

        textViewItemNameDetail = (TextView) findViewById(R.id.text_view_item_name_detail);
        textViewItemQuantityDetail = (TextView) findViewById(R.id.text_view_item_quantity_detail);
        buttonDeleteItem = (Button) findViewById(R.id.button_delete_item);

        textViewItemNameDetail.setText(itemName);
        textViewItemQuantityDetail.setText(itemQuantity);

        buttonDeleteItem.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_delete_item:
                ItemsDataSource helper = new ItemsDataSource(this);
                try {
                    boolean successDelete;
                    helper.open();
                    successDelete = helper.deleteItem(itemId);
                    helper.close();

                    if (successDelete)
                        finish();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}