package com.example.voyager.testapp.sample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.voyager.testapp.sample.db.tables.UsersTable;
import com.example.voyager.testapp.sample.model.User;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by voyager on 10/23/15.
 */
public class UsersDataSource {

    private SQLiteDatabase db;
    private MySQLiteHelper helper;

    public UsersDataSource(Context context) {
        helper = new MySQLiteHelper(context);
    }

    public void open () throws SQLException{
        db = helper.getWritableDatabase();
    }

    public void close () throws SQLException{
        helper.close();
    }

    public boolean insertUser(String name, String username, String password) {
        ContentValues values = new ContentValues();

        values.put(UsersTable.COLUMN_NAME, name);
        values.put(UsersTable.COLUMN_USERNAME, username);
        values.put(UsersTable.COLUMN_PASSWORD, password);

        long id = db.insert(UsersTable.TABLE_NAME, null, values);

        return id != -1;
    }

    public User getUser(String username, String password) {
        User user= null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + UsersTable.TABLE_NAME
                + " WHERE " + UsersTable.COLUMN_USERNAME + " = '" + username + "'"
                + "AND " + UsersTable.COLUMN_PASSWORD + " = '" + password + "'", null);

        Log.i("TAG", String.valueOf(cursor.getCount()));

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            int id = cursor.getInt(cursor.getColumnIndex(UsersTable.COLUMN_ID));
            String name = cursor.getString(cursor.getColumnIndex(UsersTable.COLUMN_NAME));
            user = new User(id, name, username, password);
        }

        return user;
    }

    public void deleteAllUsers() {
        db.delete(UsersTable.TABLE_NAME, null, null);
    }
}