package com.example.voyager.testapp.sample.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.db.UsersDataSource;
import com.example.voyager.testapp.sample.model.User;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by sydong on 02/05/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private Button buttonContinue;
    private TextView textViewRegister;

    private String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = (EditText) findViewById(R.id.login_username_edit_text);
        editTextPassword = (EditText) findViewById(R.id.login_password_edit_text);
        buttonContinue = (Button) findViewById(R.id.login_button);
        textViewRegister = (TextView) findViewById(R.id.login_register_text_view);

        buttonContinue.setOnClickListener(this);
        textViewRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                username = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();

                if (username.equals("") || password.equals(""))
                    Toast.makeText(LoginActivity.this, "Please fill all the fields", Toast.LENGTH_SHORT).show();
                else
                    loginUser();
                break;
            case R.id.login_register_text_view:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }

    private void loginUser() {
        UsersDataSource helper = new UsersDataSource(this);

        User user = null;
        try {
            helper.open();
            user = helper.getUser(username, password);
            helper.close();
        } catch (SQLException e){
            e.printStackTrace();
        }

        if (user == null) {
            Toast.makeText(this, "Invalid username/password", Toast.LENGTH_SHORT).show();
        } else {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}