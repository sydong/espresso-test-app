package com.example.voyager.testapp.sample.fragments;

import android.app.Activity;
import android.content.Intent;
import android.view.View.OnClickListener;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.activities.LoginActivity;
import com.example.voyager.testapp.sample.constants.Constants;

/**
 * Created by voyager on 11/2/15.
 */
public class HomeFragment extends Fragment implements OnClickListener {

    private View view;

    private Button showToastMessageButton;
    private Button showHideHeaderButton;
    private Button buttonLogout;

    private TextView textViewHeader;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);

        showToastMessageButton = (Button) view.findViewById(R.id.toast_message_button);
        showHideHeaderButton = (Button) view.findViewById(R.id.button_show_hide_header);
        buttonLogout = (Button) view.findViewById(R.id.button_logout);

        textViewHeader = (TextView) view.findViewById(R.id.text_view_header);

        showToastMessageButton.setOnClickListener(this);
        showHideHeaderButton.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);

        showHideHeaderButton.setText(getResources().getString(R.string.button_show_header));

        if (textViewHeader.getVisibility() == View.VISIBLE)
            showHideHeaderButton.setText(getResources().getString(R.string.button_hide_header));

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toast_message_button:
                showToastMessage(Constants.TOAST_MESSAGE_MESSAGE);
                break;
            case R.id.button_show_hide_header:
                showHideTextHeader();
                break;
            case R.id.button_logout:
                logout();
                break;
        }
    }

    private void logout() {
        showToastMessage("You have been logged out.");
        startActivity(new Intent(getActivity(), LoginActivity.class));
        getActivity().finish();
    }

    private void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void showHideTextHeader() {
        boolean isVisible = textViewHeader.isShown();

        if (isVisible) {
            textViewHeader.setVisibility(View.INVISIBLE);
            showHideHeaderButton.setText(view.getResources().getString(R.string.button_show_header));
        } else {
            textViewHeader.setVisibility(View.VISIBLE);
            showHideHeaderButton.setText(view.getResources().getString(R.string.button_hide_header));
        }
    }
}