package com.example.voyager.testapp.sample.activities;

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.constants.Constants;
import com.example.voyager.testapp.sample.fragments.DBFragment;
import com.example.voyager.testapp.sample.fragments.ContactsFragment;
import com.example.voyager.testapp.sample.fragments.WebviewFragment;
import com.example.voyager.testapp.sample.fragments.ItemsListFragment;
import com.example.voyager.testapp.sample.fragments.HomeFragment;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener{

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;

    static private FragmentManager fm;
    private ListView listViewNavItems;

    private String[] navItems;

    private ArrayAdapter<String> adapter;

    private RelativeLayout drawerContainer;

    private static int fragmentTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listViewNavItems = (ListView) findViewById(R.id.list_view_navigation_menu);
        drawerContainer = (RelativeLayout) findViewById(R.id.drawer_container);

        navItems = getResources().getStringArray(R.array.nav_items);

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navItems);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        fm = getSupportFragmentManager();

        listViewNavItems.setOnItemClickListener(this);

        listViewNavItems.setAdapter(adapter);

        toggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.app_name,
                R.string.app_name)
        {
            @Override
            public void onDrawerClosed(View drawerView) {
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };

        drawerLayout.setDrawerListener(toggle);

        getSupportActionBar().setTitle(navItems[0]);
        changeFragment(Constants.FRAGMENT_HOME);
    }

    public static void changeFragment(int _fragmentTag) {

        fragmentTag = _fragmentTag;

        Fragment fragment = null;

        switch (_fragmentTag) {
            case Constants.FRAGMENT_HOME:
                fragment = new HomeFragment();
                break;
            case Constants.FRAGMENT_1:
                fragment = new DBFragment();
                break;
            case Constants.FRAGMENT_2:
                fragment = new ContactsFragment();
                break;
            case Constants.FRAGMENT_3:
                fragment = new WebviewFragment();
                break;
            case Constants.FRAGMENT_4:

                break;
            case Constants.FRAGMENT_5:

                break;
            case Constants.FRAGMENT_6:

                break;
            case Constants.FRAGMENT_ITEM_LIST:
                fragment = new ItemsListFragment();
                break;
        }

        if (fragment != null) {
            FragmentTransaction transaction = fm.beginTransaction();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (toggle.onOptionsItemSelected(item))
            return true;

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = drawerLayout.isDrawerOpen(drawerContainer);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getSupportActionBar().setTitle(navItems[position]);
        changeFragment(position);
        drawerLayout.closeDrawer(drawerContainer);
    }

    @Override
    public void onBackPressed() {

        if (fragmentTag == Constants.FRAGMENT_HOME)
            finish();
        else
            fm.popBackStack();
    }
}
