package com.example.voyager.testapp.sample.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.voyager.testapp.sample.R;

/**
 * Created by voyager on 11/3/15.
 */
public class WebviewFragment extends Fragment {

    public static final String KEY_URL_TO_LOAD = "KEY_URL_TO_LOAD";

    public static final String WEB_FORM_URL = "https://www.takatack.com/";

    private View view;

    private WebView webView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_webview, container, false);

        webView = (WebView) view.findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new Callback());
        webView.loadUrl(urlFromIntent(getActivity().getIntent()));
        webView.requestFocus();

        return view;
    }

    private class Callback extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return (false);
        }

    }

    private static String urlFromIntent(@NonNull Intent intent) {
        //checkNotNull(intent, "Intent cannot be null!");
        String url = intent.getStringExtra(KEY_URL_TO_LOAD);
        return !TextUtils.isEmpty(url) ? url : WEB_FORM_URL;
    }
}
