package com.example.voyager.testapp.sample.db.tables;

/**
 * Created by sydong on 28/04/2016.
 */
public class ItemsTable {

    public static final String TABLE_NAME = "items";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ITEM_NAME = "item_name";
    public static final String COLUMN_ITEM_QUANTITY = "quantity";

    public static final String CREATE_TABLE = "CREATE TABLE "
            + TABLE_NAME + " ("
            + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COLUMN_ITEM_NAME + " TEXT,"
            + COLUMN_ITEM_QUANTITY + " INTEGER);";
}