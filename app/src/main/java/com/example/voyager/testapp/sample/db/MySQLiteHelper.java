package com.example.voyager.testapp.sample.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.voyager.testapp.sample.db.tables.ItemsTable;
import com.example.voyager.testapp.sample.db.tables.UsersTable;

/**
 * Created by voyager on 10/23/15.
 */
public class MySQLiteHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "dbItems.db";
    private static final int DATABASE_VERSION = 1;

    public MySQLiteHelper (Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ItemsTable.CREATE_TABLE);
        db.execSQL(UsersTable.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ItemsTable.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + UsersTable.TABLE_NAME);

        onCreate(db);
    }


}
