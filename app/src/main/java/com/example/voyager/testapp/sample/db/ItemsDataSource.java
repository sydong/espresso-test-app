package com.example.voyager.testapp.sample.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.voyager.testapp.sample.db.tables.ItemsTable;
import com.example.voyager.testapp.sample.model.Item;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by voyager on 10/23/15.
 */
public class ItemsDataSource {

    private SQLiteDatabase db;
    private MySQLiteHelper helper;

    public ItemsDataSource(Context context) {
        helper = new MySQLiteHelper(context);
    }

    public void open () throws SQLException{
        db = helper.getWritableDatabase();
    }

    public void close () throws SQLException{
        helper.close();
    }

    public boolean insertItem(String itemName, int quantity) {
        ContentValues values = new ContentValues();

        values.put(ItemsTable.COLUMN_ITEM_NAME, itemName);
        values.put(ItemsTable.COLUMN_ITEM_QUANTITY, quantity);

        long id = db.insert(ItemsTable.TABLE_NAME, null, values);

        return id != -1;
    }

    public boolean reduceItemQuantity(int itemID, int updatedItemQuantity) {

        ContentValues values = new ContentValues();

        values.put(ItemsTable.COLUMN_ITEM_QUANTITY, updatedItemQuantity);

        return db.update(ItemsTable.TABLE_NAME, values, ItemsTable.COLUMN_ID + " = " + itemID, null) > 0;
    };

    public Item getItem(int itemID) {
        Item item = null;

        Cursor cursor = db.rawQuery("SELECT * FROM " + ItemsTable.TABLE_NAME + " WHERE " + ItemsTable.COLUMN_ID + " = '" + itemID + "'", null);

        Log.i("TAG", String.valueOf(cursor.getCount()));

        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            String itemName = cursor.getString(cursor.getColumnIndex(ItemsTable.COLUMN_ITEM_NAME));
            int itemQuantity = cursor.getInt(cursor.getColumnIndex(ItemsTable.COLUMN_ITEM_QUANTITY));

            item = new Item(itemID, itemName, itemQuantity);
        }

        return item;
    }

    public ArrayList<Item> getAllItems() {
        ArrayList<Item> items = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + ItemsTable.TABLE_NAME, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(ItemsTable.COLUMN_ID));
            String itemName = cursor.getString(cursor.getColumnIndex(ItemsTable.COLUMN_ITEM_NAME));
            int quantity = cursor.getInt(cursor.getColumnIndex(ItemsTable.COLUMN_ITEM_QUANTITY));

            items.add(new Item(id, itemName, quantity));
            cursor.moveToNext();
        }

        return items;
    }

    public void deleteAllItems() {
        db.delete(ItemsTable.TABLE_NAME, null, null);
    }

    public boolean deleteItem(int itemId) {

        if (itemId == -1)
            return false;

        return db.delete(ItemsTable.TABLE_NAME, ItemsTable.COLUMN_ID + "='" + itemId + "'", null) > 0;
    }
}