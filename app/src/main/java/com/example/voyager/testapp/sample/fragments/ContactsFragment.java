package com.example.voyager.testapp.sample.fragments;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.constants.Constants;

import java.io.File;

/**
 * Created by voyager on 11/3/15.
 */
public class ContactsFragment extends Fragment implements OnClickListener{

    private View view;

    private Button buttonPickContact;
    private Button buttonCaptureImage;

    private TextView textViewContactName;
    private TextView textViewContactNumber;

    private ImageView imageViewPreviewImage;

    private Uri fileUri;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_contacts, container, false);

        buttonPickContact = (Button) view.findViewById(R.id.button_pick_contact);
        buttonCaptureImage = (Button) view.findViewById(R.id.button_capture_image);

        textViewContactName = (TextView) view.findViewById(R.id.text_view_contact_name);
        textViewContactNumber = (TextView) view.findViewById(R.id.text_view_contact_number);

        imageViewPreviewImage = (ImageView) view.findViewById(R.id.image_view_preview_image);

        buttonPickContact.setOnClickListener(this);
        buttonCaptureImage.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_pick_contact:
                pickContact();
                break;
            case R.id.button_capture_image:
                captureImage();
                break;
        }
    }

    private void pickContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(pickContactIntent, Constants.REQUEST_CODE_PICK_CONTACT);
    }

    private void captureImage() {
        Intent captureImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File imagesFolder = new File(Environment.getExternalStorageDirectory(), "EspressoTestAppImages");
        imagesFolder.mkdirs();

        File image = new File(imagesFolder, "image_" + System.currentTimeMillis() + ".jpg");

        fileUri = Uri.fromFile(image);

        captureImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(captureImageIntent, Constants.REQUEST_CODE_CAPTURE_IMAGE);
    }

    private void previewCapturedImage() {

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            options.inSampleSize = 8;

            Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(), options);

            imageViewPreviewImage.setImageBitmap(bitmap);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_PICK_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                Uri contactsUri = data.getData();

                String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER};

                Log.i("TAG", contactsUri.toString());

                Cursor cursor = getActivity().getContentResolver().query(contactsUri, projection, null, null, null);
                cursor.moveToFirst();

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                textViewContactName.setText(name);
                textViewContactNumber.setText(contactNumber);
            }
        } else if (requestCode == Constants.REQUEST_CODE_CAPTURE_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                previewCapturedImage();
            }
        }
    }
}
