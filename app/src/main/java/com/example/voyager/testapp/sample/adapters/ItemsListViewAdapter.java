package com.example.voyager.testapp.sample.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.model.Item;

import java.util.ArrayList;

/**
 * Created by voyager on 11/3/15.
 */
public class ItemsListViewAdapter extends BaseAdapter{

    private ArrayList<Item> items;

    private LayoutInflater inflater;

    public ItemsListViewAdapter(Context context, ArrayList<Item> items) {
        this.items = items;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View row, ViewGroup parent) {
        row = inflater.inflate(R.layout.list_item, parent, false);

        TextView textViewItemName = (TextView) row.findViewById(R.id.text_view_item_name);
        TextView textViewItemQuantity = (TextView) row.findViewById(R.id.text_view_item_quantity);

        Item item = (Item) getItem(position);

        textViewItemName.setText("ID: " + item.getId() + "\n" + "Name: " + item.getItemName());
        textViewItemQuantity.setText(String.valueOf(item.getQuantity()));

        return row;
    }
}
