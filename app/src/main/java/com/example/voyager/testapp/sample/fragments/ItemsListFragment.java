package com.example.voyager.testapp.sample.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.activities.ItemDetailsActivity;
import com.example.voyager.testapp.sample.adapters.ItemsListViewAdapter;
import com.example.voyager.testapp.sample.constants.Constants;
import com.example.voyager.testapp.sample.db.ItemsDataSource;
import com.example.voyager.testapp.sample.model.Item;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by voyager on 11/3/15.
 */
public class ItemsListFragment extends Fragment implements AdapterView.OnItemClickListener{

    private View view;

    private ListView listView;
    private TextView textViewError;

    private ItemsListViewAdapter adapter;

    private ItemsDataSource helper;

    private ArrayList<Item> items;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_item_list, container, false);

        listView = (ListView) view.findViewById(R.id.list_view_items);
        textViewError = (TextView) view.findViewById(R.id.text_view_error_message);

        return view;
    }

    private void showErrorMessage (String message) {
        listView.setVisibility(View.GONE);
        textViewError.setVisibility(View.VISIBLE);

        textViewError.setText(message);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(getActivity(), ItemDetailsActivity.class);

        intent.putExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_NAME_EXTRA, items.get(position).getItemName());
        intent.putExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_QUANTITY_EXTRA, items.get(position).getQuantity());
        intent.putExtra(Constants.ITEM_DETAILS_ACTIVITY_ITEM_ID_EXTRA, items.get(position).getId());

        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();

        helper = new ItemsDataSource(getActivity());

        getItems();


        if (items.isEmpty() || items.size() < 1)
            showErrorMessage(Constants.ERROR_MESSAGE_VIEW_ITEMS_EMPTY);
        else {
            adapter = new ItemsListViewAdapter(getActivity(), items);

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(this);

            adapter.notifyDataSetChanged();
        }

        Log.i("TAG", "ONRESUME ON FRAGMENT");
    }

    private void getItems() {
        items = new ArrayList<>();

        try {
            helper.open();
            items = helper.getAllItems();
            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
            showErrorMessage(Constants.ERROR_MESSAGE_VIEW_ITEMS_ERROR);
        }
    }
}
