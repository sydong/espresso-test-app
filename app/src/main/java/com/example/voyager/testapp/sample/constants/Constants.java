package com.example.voyager.testapp.sample.constants;

/**
 * Created by voyager on 11/2/15.
 */
public class Constants {

    public static final int FRAGMENT_HOME = 0;
    public static final int FRAGMENT_1 = 1;
    public static final int FRAGMENT_2 = 2;
    public static final int FRAGMENT_3 = 3;
    public static final int FRAGMENT_4 = 4;
    public static final int FRAGMENT_5 = 5;
    public static final int FRAGMENT_6 = 6;
    public static final int FRAGMENT_ITEM_LIST = 10;

    public static final String STATUS_MESSAGE_ADD_ITEM_SUCCESS = "Item added!";
    public static final String STATUS_MESSAGE_ADD_ITEM_FAILED = "Failed adding of item";
    public static final String STATUS_MESSAGE_ADD_ITEM_MISSING_FIELD = "Please fill all the fields";

    public static final String STATUS_MESSAGE_ITEMS_NOTHING_TO_DELETE = "Nothing to delete";
    public static final String STATUS_MESSAGE_ITEMS_DELETE_ERROR = "Error in deleting items";
    public static final String STATUS_MESSAGE_ITEMS_DELETE_SUCCESS = "All items deleted";

    public static final String ERROR_MESSAGE_VIEW_ITEMS_EMPTY = "No items added yet";
    public static final String ERROR_MESSAGE_VIEW_ITEMS_ERROR = "Unable to get items";

    public static final String TOAST_MESSAGE_MESSAGE = "This is a toast message";

    public static final int REQUEST_CODE_PICK_CONTACT = 100;
    public static final int REQUEST_CODE_CAPTURE_IMAGE = 200;

    public static final String ITEM_DETAILS_ACTIVITY_ITEM_ID_EXTRA = "itemId";
    public static final String ITEM_DETAILS_ACTIVITY_ITEM_NAME_EXTRA = "itemName";
    public static final String ITEM_DETAILS_ACTIVITY_ITEM_QUANTITY_EXTRA = "itemQuantity";
}
