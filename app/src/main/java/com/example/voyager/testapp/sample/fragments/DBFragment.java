package com.example.voyager.testapp.sample.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.activities.MainActivity;
import com.example.voyager.testapp.sample.constants.Constants;
import com.example.voyager.testapp.sample.db.ItemsDataSource;
import com.example.voyager.testapp.sample.model.Item;

import java.sql.SQLException;

/**
 * Created by voyager on 11/3/15.
 */
public class DBFragment extends Fragment implements OnClickListener{

    private View view;

    private EditText editTextItemName;
    private EditText editTextItemQuantity;

    private Button buttonAddItem;
    private Button buttonViewItems;
    private Button buttonDeleteAllItems;
    private Button buttonGetItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_db, container, false);

        editTextItemName = (EditText) view.findViewById(R.id.edit_text_item_name);
        editTextItemQuantity = (EditText) view.findViewById(R.id.edit_text_item_quantity);

        buttonAddItem = (Button) view.findViewById(R.id.button_add_item);
        buttonViewItems = (Button) view.findViewById(R.id.button_view_items);
        buttonDeleteAllItems = (Button) view.findViewById(R.id.button_delete_all_items);
        buttonGetItem = (Button) view.findViewById(R.id.button_get_item);

        buttonAddItem.setOnClickListener(this);
        buttonViewItems.setOnClickListener(this);
        buttonDeleteAllItems.setOnClickListener(this);
        buttonGetItem.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_item:
                if (editTextItemName.getText().toString().equals("") || editTextItemQuantity.getText().toString().equals(""))
                    showToastMessage(Constants.STATUS_MESSAGE_ADD_ITEM_MISSING_FIELD);
                else
                    addItem();
                break;
            case R.id.button_view_items:
                MainActivity.changeFragment(Constants.FRAGMENT_ITEM_LIST);
                break;
            case R.id.button_delete_all_items:
                if (getNumberOfItems() < 1)
                    showToastMessage(Constants.STATUS_MESSAGE_ITEMS_NOTHING_TO_DELETE);
                else deleteAllItems();
                break;
            case R.id.button_get_item:
                showGetItemsDialog().show();
                break;
        }
    }

    private void addItem() {

        boolean success;

        String name = editTextItemName.getText().toString();
        int quantity = Integer.parseInt(editTextItemQuantity.getText().toString());
        ItemsDataSource helper = new ItemsDataSource(getActivity());

        try {
            helper.open();
            success = helper.insertItem(name, quantity);
            helper.close();
            clearFields();
        } catch (SQLException e) {
            e.printStackTrace();
            success = false;
        }

        String message = success ? Constants.STATUS_MESSAGE_ADD_ITEM_SUCCESS : Constants.STATUS_MESSAGE_ADD_ITEM_FAILED;

        showToastMessage(message);
    }

    private void deleteAllItems() {

        boolean success;

        ItemsDataSource helper = new ItemsDataSource(getActivity());

        try {
            helper.open();
            helper.deleteAllItems();
            success = true;
            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
            success = false;
        }

        String message = success ? Constants.STATUS_MESSAGE_ITEMS_DELETE_SUCCESS : Constants.STATUS_MESSAGE_ITEMS_DELETE_ERROR;

        showToastMessage(message);
    }

    private int getNumberOfItems() {
        ItemsDataSource helper = new ItemsDataSource(getActivity());

        int count = 0;

        try {
            helper.open();
            count = helper.getAllItems().size();
            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return count;
    }

    private void showToastMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    private void clearFields() {
        editTextItemName.setText("");
        editTextItemQuantity.setText("");
    }

    private void getItem(int itemID, int itemQuantity) {
        ItemsDataSource helper = new ItemsDataSource(getActivity());

        try {
            helper.open();
            Item item = helper.getItem(itemID);

            if (item == null) {
                showToastMessage("No item found");
            } else {
                if (item.getQuantity() >= itemQuantity) {
                    boolean success = helper.reduceItemQuantity(itemID, item.getQuantity() - itemQuantity);
                    showToastMessage(success ? "Success" : "Failed");
                } else {
                    showToastMessage("Insufficient quantity");
                }
            }

            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Dialog showGetItemsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_get_item, null);

        final EditText editTextItemID = (EditText) view.findViewById(R.id.edit_text_item_id);
        final EditText editTextItemQuantity = (EditText) view.findViewById(R.id.edit_text_item_quantity);

        builder.setView(view)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String itemID = editTextItemID.getText().toString();
                        String itemQuantity = editTextItemQuantity.getText().toString();

                        getItem(Integer.parseInt(itemID), Integer.parseInt(itemQuantity));
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }
}