package com.example.voyager.testapp.sample.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.voyager.testapp.sample.R;
import com.example.voyager.testapp.sample.db.UsersDataSource;

import java.sql.SQLException;

/**
 * Created by sydong on 02/05/2016.
 */
public class RegisterActivity extends Activity implements View.OnClickListener{

    private EditText editTextName;
    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button buttonRegister;

    private String name, username, password, confirmPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextName = (EditText) findViewById(R.id.register_name_edit_text);
        editTextUsername = (EditText) findViewById(R.id.register_username_edit_text);
        editTextPassword = (EditText) findViewById(R.id.register_password_edit_text);
        editTextConfirmPassword = (EditText) findViewById(R.id.register_confirm_password_edit_text);

        buttonRegister = (Button) findViewById(R.id.register_button);

        buttonRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_button:
                name = editTextName.getText().toString();
                username = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();
                confirmPassword = editTextConfirmPassword.getText().toString();

                if (areAllFieldsFilled()) {
                    if (password.equals(confirmPassword))
                        if (password.length() >= 7)
                            registerUser();
                        else
                            Toast.makeText(this, "Password length should be 7 or more", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(this, "Passwords do not match.", Toast.LENGTH_SHORT).show();
                }
                else
                    Toast.makeText(this, "Please fill all the fields.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void registerUser() {
        UsersDataSource helper = new UsersDataSource(this);

        boolean success = false;
        try {
            helper.open();
            success = helper.insertUser(name, username, password);
            helper.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Toast.makeText(this, success ? "Registration success!" : "System Error!", Toast.LENGTH_SHORT).show();

        if (success) finish();
    }

    private boolean areAllFieldsFilled() {
        return !(name.equals("") ||
                    username.equals("") ||
                    password.equals("") ||
                    confirmPassword.equals(""));
    }
}