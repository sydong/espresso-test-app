package com.example.voyager.testapp.sample;

import android.support.test.rule.ActivityTestRule;

import com.example.voyager.testapp.sample.activities.MainActivity;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * Created by MCLagare on 2/18/16.
 */
public class FirstTestClass {

    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule(MainActivity.class);

    @BeforeClass
    public static void suiteSetUp() {

    }

    @Before
    public void testSetUp() {

    }

    @Test
    public void checkHomeFragmentTestHeader() throws InterruptedException {

//        onView(withId(R.id.text_view_header)).check(matches(withText("Home Fragment")));
        onView(withId(R.id.toast_message_button)).perform(click());
        onView(withText("This is a toast message"))
                .inRoot(withDecorView(not(is(rule.getActivity().getWindow().getDecorView()))))
                .check(matches(isDisplayed()));

        Thread.sleep(3000);
    }

    @After
    public void testTeardown() {

    }

    @AfterClass
    public static void suiteTeardown() {

    }
}